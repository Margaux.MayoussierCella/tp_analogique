﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MccDaq;
using LibAnalogTools;
using System.Threading;

namespace LibAnalogTools
{




    class AnalogInputThread
    {
        private MccBoard acqBoard;

        private ushort mesureBrute { get => MesureBrute;  set { } }
        private float mesureVolt {  get => MesureVolt;  set { } }

        public readonly int Channel;
        public readonly ushort MesureBrute;
        public readonly float MesureVolt;

        public int PeriodeEchantillonage { get => PeriodeEchantillonage; set { } }
        public readonly string nom;

        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);


        public AnalogInputThread(MccBoard board, int channel,int periodeEchMs , string nom)
        {

            this.acqBoard = board;
            channel = 0;

        }


    public void runMesure()
        {
            //acquisition dans un thread.

            while (!SignalExit.WaitOne())
            {



                //on pose un verrou pour s'assurer qu'un seul objet à la fois n'y accède
                lock (verrou)
                {

                }

                
            }

        }

    public void Start()
        {

        }

    public void Stop()
        {
            SignalExit.Set();
        }

    public void Join()
        {

        }
    }


}
