﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class SimpleThreadingExample
    {


        static void Main(string[] args)
        {
            int iter = 15;


            //////////////////////////// THREAD LETTER /////////////////////////////////////
            // Création des objets encapsulant les threads.
            MyThreadLetter ThLetter = new MyThreadLetter();
            Console.WriteLine("> Le thread Letter à été créé.");

            ThLetter.Start();

            Console.WriteLine("> En route pour {0}sec. ...", iter);
            for (int i = 0; i < iter; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> main: " + i + "sec.");
            }

            Console.WriteLine("> Demmande d’arrêt du thread Letter.");
            // Beurk! Abort c'est mal !!
            //ThLetter.LaTache.Abort();
            // On remplace : ThLetter.Stop = true;
            ThLetter.SignalExit.Set();


            // On attend que le thread en cours soit terminés avant de continuer.
            ThLetter.LaTache.Join();



            //////////////////////////// THREAD NUMBER /////////////////////////////////////
           
            //MyThreadNumber ThCptr = new MyThreadNumber();
            //Console.WriteLine(" > Le thread Number à été créé");

            //ThCptr.Start(19);

            //Console.WriteLine("> Demmande d’arrêt du thread Number.");
            //// On remplace : ThCptr.Stop = true;
            //ThCptr.SignalExit.Set();
            //ThCptr.LaTache.Join();



            ///////////////////////////// Thread Compteur de Voyelle ////////////////////////
            MyThreadCompteurVoyelles ThVoyelle = new MyThreadCompteurVoyelles();
            Console.WriteLine(" > Le thread Voyelle à été créé");

            ThVoyelle.Start(19);

            Console.WriteLine("> Demmande d’arrêt du thread CompteurVoyelle.");
            ThVoyelle.SignalExit.Set();
            ThVoyelle.LaTache.Join();



            Console.WriteLine("> Les threads sont terminés.");
            Console.WriteLine("\n\n\t\tTouche \"Entrée\" pour terminer...");
            Console.ReadLine();

        }



    }


    
}
