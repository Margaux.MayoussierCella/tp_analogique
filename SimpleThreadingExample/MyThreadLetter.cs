﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadLetter
    {

        private bool _stop;
        public bool Stop
        {
            get => _stop;
            set => _stop = false;
        }


        private char Letter { get; set; }

        Random rnd = new Random();

        public Thread LaTache { get; set; }


        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);

        public AutoResetEvent SignalVoyelle { get; } = new AutoResetEvent(false);

        //Encapsulation du thread
        private void RunLetter()
        {
            Console.WriteLine("\t\t---> Le threadLetter commence son travail.");

            Letter = (char)rnd.Next(97, 123);
            switch (Letter)
            {
                case 'a':
                case 'e':
                case 'i':
                case 'o':
                case 'u':
                case 'y':
                    SignalVoyelle.Set();
                    break;
                default:
                    break;
            }

            //WaitOne() permet d'attednre le signal. Ici le temps d'attente est fixé aléatoirement
            // SI le n'est pas levé avant la fin de l'attente il retournera false
            // s'il retourne true il quittera le while.

            // si pas de parametre pour WaitOne l'attente = infinie ( la mettre a 0 ce n'est pas top)
            while (!SignalExit.WaitOne(rnd.Next(100, 1001), false))
            {
                if (Letter < 'z')
                    Letter++;
                else
                    Letter = 'a';

                Console.WriteLine("\t\t---> Letter: " + Letter.ToString());

                
                // Dodo pour un temps aléatoire < 1267ms
                Thread.Sleep(rnd.Next(1267));
            }
            Console.WriteLine("\t\t---> Le threadLetter a fini son travail.");
        }


        public MyThreadLetter()
        {
            // Création d'un thread, sans le démarrer
           LaTache = new Thread(new ThreadStart(RunLetter));
        }


        public void Start()
        {
            try
            {
                LaTache.Start();
                Console.WriteLine("Letter démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Letter a déjà été démarré...");
            }
        }




    }
}
