﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadCompteurVoyelles
    {

        private int Number { get; set; }

        Random rnd = new Random();

        private object verrou = new object();

        public Thread LaTache { get; set; }

        public AutoResetEvent SignalExit { get; } = new AutoResetEvent(false);

        public MyThreadLetter thLetter = new MyThreadLetter();

        private int compteurVoyelle;


        private void RunCompteurVoyelle(object max)
        {
            

            while (!SignalExit.WaitOne(rnd.Next(100, 1001), false))
            {
                if (Number < (int)max)
                    Number++;
                else
                    Number = 0;

                Console.WriteLine("=======> Number: " + Number.ToString());

                // Dodo pour un temps aléatoire < 323ms
                Thread.Sleep(rnd.Next(323));
            }
            Console.WriteLine("Le threadNumber a fini son travail.");


            // Nos signaux signalXXX sont des WaitHandle. On les range dans un tableau.
            WaitHandle[] LesSignaux = { SignalExit, thLetter.SignalVoyelle };
            int indexSignal = -1; // On aura besoin d'un index pour ce tableau
            bool fin = false; // Passera à true lorsqu'il faudra s'arréter

            // Attend que fin passe à true
            while (!fin)
            {
                // Attente bloquante d'un des signaux du tableau LesSignaux
                // indexSignal contiendra l'index dans le tableau de celui déclenché
                indexSignal = WaitHandle.WaitAny(LesSignaux);
                switch (indexSignal)
                {
                    // C'est le SignalVoyelle qui est activé
                    case 1:    
                                    //lock permet de "poser" un verrou sur une portion de code
                                    lock (verrou)
                                    {
                                        compteurVoyelle++;
                                    }
                         
                        // this.BeginInvoke(Handlers.MajAffichageDelegate, this, compteurVoyelle); // Si 5,4 fait uniquement
                        break;

                    // C'est le SignalExit qui est activé
                    case 0:
                        fin = true;
                        break;
                }
            }

           
        }

        public void Start(int max = 999)
        {
            try
            {
                LaTache.Start(max);
                Console.WriteLine("Number démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Number a déjà été demarré...");
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Number ne peut pas démarrer par manque de mémoire...");
                throw ex;
            }
        }
    }

    
}
