﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleThreadingExample
{
    class MyThreadNumber
    {

        private bool _stop;
        public bool Stop
        {
            get => _stop;
            set => _stop = false;
        }

        private int Number;
        Random rnd = new Random();
        public Thread LaTache;

        private void RunNumber(object max)
        {
            while (!Stop)
            {
                if (Number < (int)max)
                    Number++;
                else
                    Number = 0;

                Console.WriteLine("=======> Number: " + Number.ToString());

                // Dodo pour un temps aléatoire < 323ms
                Thread.Sleep(rnd.Next(323));
            }
            Console.WriteLine("Le threadNumber a fini son travail.");
        }



        public void Start(int max = 999)
        {
            try
            {
                LaTache.Start(max);
                Console.WriteLine("Number démarré correctement...");
            }
            catch (ThreadStateException ex)
            {
                Console.WriteLine("Number a déjà été demarré...");
                throw ex;
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Number ne peut pas démarrer par manque de mémoire...");
                throw ex;
            }
        }



    }
}
