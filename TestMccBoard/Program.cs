﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MccDaq;
using System.Threading;

namespace TestMccBoard
{
    class Program
    {
        static void Main(string[] args)
        {
            float option = 0;
            ushort dataValue = 0;
            float dataValueVolt = 0;
            int channellum = 0;
            int channeltemp = 0;
            Range range = Range.Bip10Volts;
            MccBoard mccBoard7 = new MccBoard(0);
            ErrorInfo ULStat;

            for (int i = 0; i < 200; i++)
            {
                channellum = 7;
                channeltemp = 6;


                 mccBoard7.AIn(channellum, range, out dataValue);
                 mccBoard7.VIn(channellum, range, out dataValueVolt, 0);
                Console.WriteLine( " Data : " + dataValue.ToString());
                Console.WriteLine(" Volts :" + dataValueVolt.ToString());



                mccBoard7.AIn(channeltemp, range, out dataValue);
                mccBoard7.VIn(channeltemp, range, out dataValueVolt, 0);
                Console.WriteLine( " Temp : " + dataValue.ToString());
                Console.WriteLine( " Volts :" + dataValueVolt.ToString());

                Thread.Sleep(1000);

               
            }

            Console.ReadKey();
        }
    }
}
