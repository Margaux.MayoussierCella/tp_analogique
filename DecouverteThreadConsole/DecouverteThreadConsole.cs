﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DecouverteThreadConsole
{
    class DecouverteThreadConsole
    {

        public static bool stop = false;


        static void Main(string[] args)
        {
            // La durée en seconde du thread principal
            int dureeMain = 20;

            // Le thread principal compte le temps
            Console.WriteLine("> Main en route pour {0} secondes...", dureeMain);
            for (int i = 1; i < dureeMain; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("> main: " + i + "sec.");
            }

            // Le thread lui même, création sans le démarrer
            ThreadStart delegateRunLetter = new ThreadStart(RunLetter);
            Thread LaTache = new Thread(delegateRunLetter);

            // Démarrage des threads
            Console.WriteLine("> Le thread est créé.\n> Démarrage...");
            LaTache.Start();

            // Arrêt du thread LaTache
            Console.WriteLine("> Demmande d'arret des threads.");
            //LaTache.Abort();
            stop = true;

            // On attend que le thread en cours soit effectivement terminé avant de continuer.
            LaTache.Join();

            Console.WriteLine("> Le thread LaTache est terminé.");
            Console.WriteLine("\n\n\t\t Touche \"Entrée\" pour terminer...");
            Console.ReadLine();

        }




        public static void RunLetter()
        {
            /// Un random pour le sleep...
            Random rnd = new Random();

            /// La lettre à afficher
            char letter = 'a';

            Console.WriteLine("\t\t---> Le RunLetter commence son travail.");
            while (!stop)
            {
                if (letter < 'z')
                    letter++;
                else
                    letter = 'a';

                Console.WriteLine("\t\t---> Letter: " + letter.ToString());

                // Dodo pour un temps aléatoire < 1267ms
                Thread.Sleep(rnd.Next(1267));
            }
            Console.WriteLine("\t\t---> Le RunLetter a fini son travail.");

        }
    }
}
